import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { DotMap } from '@ant-design/maps';

function Chart4() {
  const [data, setData] = useState([]);

  const fetchData = () => {
    fetch("https://9rrrksi29k.execute-api.us-west-1.amazonaws.com/?q3")
      .then(response => {
        return response.json()
      })
      .then(data => {
        setData(data)
      })
  }

  useEffect(() => {
    fetchData()
  }, [])

  const config = {
    map: {
      type: 'mapbox',
      style: 'dark',
      pitch: 50,
      center: [-96, 38.5],
      zoom: 9.3,
      token: 'pk.eyJ1IjoibXZhc3F1ZXpuciIsImEiOiJjbDJudmM4cWwyNHY4M2VzYmZ2aDBkMjVxIn0.ZeP44puoixoehKUAYo3AnQ',
    },
    source: {
      data: data,
      parser: {
        type: 'json',
        x: 'longitude',
        y: 'latitude',
      },
    },
    color: {
      field: 'stock',
      value: ['#82cf9c', '#10b3b0', '#2033ab'],
      scale: {
        type: 'quantize',
      },
    },
    size: {
      field: 'stock',
      value: ({ stock }) => (stock /1200),
    },
    style: {
      opacity: 0.8,
      strokeWidth: 0,
    },
    state: {
      active: {
        color: '#FFF684',
      },
    },
    autoFit: true,
    zoom: {
      position: 'topright',
    },
    scale: {
      position: 'bottomright',
    },
    tooltip: {
      items: ['name', 'stock'],
    },
    legend: {
      position: 'bottomleft',
    },
  };

  return (
    <div className='mapContainer'>
      <DotMap {...config} />
    </div>

  );
}

export default Chart4;