import React from 'react';
import './App.css';
import Header from './Header.js';
import Chart from './Chart';
import Chart2 from './Chart2';
import Chart3 from './Chart3';
import Chart4 from './Chart4';

function App() {
  return (
    <div className="app">
      <Header title='Bienvenido' />
      <div className='row'>
        <div className='column'>
        <h2>Top 5 de productos por valor</h2>
          <Chart /></div>
        <div className='column'> 
        <h2>Top 5 de productos por stock</h2>
        <Chart3 />
        </div>
      </div>
      <h2>Productos por almacen</h2>
      <Chart2 />
      <br></br>
      <h2>Distribucion de inventario por almacen</h2>
      <Chart4 />
    </div>
  );
}

export default App;
