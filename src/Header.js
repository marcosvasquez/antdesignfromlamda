import React from 'react';
import './App.css';

function Header(props){
    return (
        <div className='container'>
                <div className='logo'>
                    {props.title}
                </div>
        </div>
      );
}

export default Header;