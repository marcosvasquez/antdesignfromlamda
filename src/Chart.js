import React, { useState, useEffect } from 'react';
import { Rose } from '@ant-design/plots';

function Chart() {

  const [data, setData] = useState([]);

  useEffect(() => {
    asyncFetch();
  }, []);

  const asyncFetch = () => {
    fetch('https://9rrrksi29k.execute-api.us-west-1.amazonaws.com/?q2')
      .then((response) => response.json())
      .then((json) => setData(json))
      .catch((error) => {
        console.log('fetch data failed', error);
      });
  };
  const config = {
    data,
    xField: 'name',
    yField: 'Value',
    seriesField: 'type',
    radius: 0.9,
    legend: {
      position: 'bottom',
    },
  };
  return (
    <Rose {...config} />
  );
}

export default Chart;