import React, { useState, useEffect } from 'react';
import { HexbinMap } from '@ant-design/maps';



function Chart2() {
  const [data, setData] = useState([]);

  const fetchData = () => {
    fetch("https://9rrrksi29k.execute-api.us-west-1.amazonaws.com/?q3")
      .then(response => {
        return response.json()
      })
      .then(data => {
        setData(data)
      })
  }

  useEffect(() => {
    fetchData()
  }, [])

  

 

    const config = {
    map: {
      type: 'mapbox',
      style: 'dark',
      pitch: 50,
      center: [-96, 38.5],
      zoom: 9.3,
      token: 'pk.eyJ1IjoibXZhc3F1ZXpuciIsImEiOiJjbDJudmM4cWwyNHY4M2VzYmZ2aDBkMjVxIn0.ZeP44puoixoehKUAYo3AnQ',
    },
    shape: 'hexagonColumn',
    size: {
      field: 'sum',
      value: ({ sum }) => {
        return sum * 1;
      },
    },
    source: {
      data: data,
      parser: {
        type: 'json',
        x: 'longitude',
        y: 'latitude',
      },
      aggregation: {
        field: 'sku',
        radius: 2500,
        method: 'sum',
      },
    },
    color: {
      field: 'sum',
      value: [
        '#094D4A',
        '#146968',
        '#1D7F7E',
        '#289899',
        '#34B6B7',
        '#4AC5AF',
        '#5FD3A6',
        '#7BE39E',
        '#A1EDB8',
        '#C3F9CC',
        '#DEFAC0',
        '#ECFFB1',
        '#FDF49B',
        '#F323D1',
      ],
    },
    style: {
      coverage: 0.8,
      angle: 0,
      opacity: 1.0,
    },
  };

  return (
    <div className='mapContainer'>
      <HexbinMap {...config} />
    </div>

  );
}

export default Chart2;