
## Uso de graficos AntV Design y funciones Lambda.

## Descripcion 
Mediante la ejecucion de un servicio API Gateway de AWS, que recibe un parametro por URL se ejecuta el siguiente codigo:

```
import json
import boto3

rds_client = boto3.client('rds-data')

database_name = 'antDesignTest'
db_cluster_arn = '############'
db_credentials_secrets_arn = '########'


def lambda_handler(event, context):
    newVariable = event['rawQueryString']
    if newVariable == 'q1':
        response = execute_statement('SELECT name AS Name, SUM(stock) AS Value FROM product GROUP BY name LIMIT 10 OFFSET 0 ;');
        resultFormated = parse_data_service_response(response)
        return resultFormated;
    elif newVariable == 'q2':
        response = execute_statement('SELECT name AS Name, SUM(price) AS Value FROM product GROUP BY name LIMIT 7 OFFSET 0 ;');
        resultFormated = parse_data_service_response(response)
        return resultFormated;
    elif newVariable == 'q3':
        response = execute_statement('SELECT product.sku, product.name, product.price, product. stock, warehouse.name, ST_X(adress.coordinate) AS longitude, ST_Y(adress.coordinate) AS latitude  FROM product,warehouse,warehouse_product,adress WHERE warehouse_product.id_warehouse=warehouse.id_warehouse AND warehouse_product.sku=product.sku AND warehouse.id_adress=adress.id_adress;');
        resultFormated = parse_data_service_response(response)
        return resultFormated;

def execute_statement(sql):
    response = rds_client.execute_statement(
        secretArn = db_credentials_secrets_arn,
        database = database_name,
        resourceArn = db_cluster_arn,
        sql = sql,
        includeResultMetadata=True
    )
    return response;

def parse_data_service_response(res):
    columns = [column['name'] for column in res['columnMetadata']]

    parsed_records = []
    for record in res['records']:
        parsed_record = {}
        for i, cell in enumerate(record):
            key = columns[i]
            value = list(cell.values())[0]
            parsed_record[key] = value
        parsed_records.append(parsed_record)

    return parsed_records


```

Mediante el cual se ejecutan tres distintos tipos de consultas a una base de datos serverless que tambien se encuentra implementada en AWS, el resultado es formateado por la funcion "parse_data_service_response" para agregar los nombres de las columnas como identificadores en el resultado JSON.

Este resultado es procesado por el correspondiente componente en esta aplicacion y con ello se muestra una grafica.



## Imagenes

El diagrama entidad relacion de la base de datos es el siguiente:

![Alt-Text](img/DBDesign.png)

Mientras que el resultado del tablero se muestra a continuacion.

![Alt-Text](img/DashboardExaMPLE.png)

